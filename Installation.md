# Bootstrap Mini-Course Installation



## Getting started
First of all, please creaste the following folders in your local directory 
```
cd $HOME
mkdir install 
mkdir hyperion-projects 
mkdir simpleboot 
```
We will install most of the libraries in `$HOME/install` and build `scalars-3d`, `fermions-3d` and `dynamical-sdp` inside hyperion. 


- For those without `stack` installed, please follow the instruction here https://docs.haskellstack.org/en/stable/install_and_upgrade/ and be sure to include the directory where the `stack` executables are installed in your `PATH`. This can be done by editing the ~/.bashrc file.
- For those using **Caltech and PI-Symmetry**, you can skip any installation of sdpb, blocks_3d and scalar_blocks 
- For those using **Yale Grace**, you need to install a new version of elemental library, and then compile the branch called skydiving_release of sdpb 
- For those using **Harvard FASRC**, we have installed sdpb, blocks_3d and scalar_blocks. In principle, you should be able to install and run hyperion. For simpleboot, however, we encountered some issues calling the c++ executables. We recommend you follow the steps to install hyperion and simpleboot and contact us if you encounter the same issue. 
- For **others**, you need to go through the entire installations. 
- To keep your own version of repository, I recommend you **fork** the original repository following the instructions https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html. By folking, you keep a remote copy of your own and keep track of your own projects. Works from forks can be merged into the original repository easily with merge requests. This will be particularly helpful working with `scalars-3d`, `fermions-3d` and `dynamical-sdp`. 

We understand that it is not a light task to install all the packages listed below in a limited amount of time. If you encounter difficulties, please feel free to contact us. Furthermore, **if you feel that you might not finish the installations in time**, please make sure that you have `stack` installed, and then jump to the section **Hyperion Installation** to install and compile `scalars-3d`, `fermions-3d` and `dynamical-sdp`. These should make sure that you can follow the tutorials in the afternoons. 


## SDPB, Scalar-blocks, Blocks-3d Installation 
### sdpb 
This sdpb repository [https://github.com/AikeLiu/sdpb](https://github.com/AikeLiu/sdpb/) is forked from [https://github.com/davidsd/sdpb](https://github.com/davidsd/sdpb). It contains what you need for the regular sdpb solver (master branch) and the skydiving algorithm (skydiving_release branch). 

Different versions of sdpb's are required for simpleboot, which are contained in Ning's fork of sdpb [https://github.com/suning-git/sdpb](https://github.com/suning-git/sdpb). It also contains what you need for skydiving. Hence, if you are installing both skydiving and simpleboot, we recommend you to clone from [https://github.com/suning-git/sdpb](https://github.com/suning-git/sdpb). 

When you install those packages, please keep a note about the address of all executables, as later we will need the location of the executables to set up simpleboot and hyperion.

Please find general instructions and instructions specific to differnt HPCs in [sdpb/Install.md](https://github.com/AikeLiu/sdpb/blob/master/Install.md) and [sdpb/tree/master/docs/site_installs](https://github.com/AikeLiu/sdpb/tree/master/docs/site_installs). 

**To compile the skydiving algorithm**, you will need a modified verision of `elemental` library. Please clone this repository
[https://gitlab.com/AikeLiu/elemental](https://gitlab.com/AikeLiu/elemental/-/tree/MPFR_LogExp) and make sure that you switch to the branch MPFR_LogExp. And follow the following lines to compile. Notice that depending on your environment, you might need to specify the location of the MPFR library using cmake flags. You can find some example compilation command at the end of this file. 

```
cd $HOME
git clone https://gitlab.com/AikeLiu/elemental.git libelemental
cd libelemental
git checkout MPFR_LogExp
# to make sure that you are on the correct branch
git branch 
# this should return
* MPFR_LogExp
  master
# Then build the library following 
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/install -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc
make install
```
After reinstalling `elemental`, follow the rest of instructions in the [sdpb/tree/master/docs/site_installs](https://github.com/AikeLiu/sdpb/tree/master/docs/site_installs). In particular, make sure to install the following version of the formatting library `fmt` as given in the instruction. 
```
wget https://github.com/fmtlib/fmt/releases/download/6.2.1/fmt-6.2.1.zip
mkdir build
cd build
cmake -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=$HOME/install ..
```
The newest fmt library is not compatible with current version of fermions-3d. 

To compile the skydiving executable, you need to switch to a different branch. 
```
git checkout skydiving-release 
./waf 
```

### scalar_blocks
 The package scalar_blocks [https://gitlab.com/bootstrapcollaboration/scalar_blocks](https://gitlab.com/bootstrapcollaboration/scalar_blocks) is a C++ library computing scalar conformal blocks in general dimension d. You can find installation instuctions following this link [scalar_blocks/Install.md](/https://gitlab.com/bootstrapcollaboration/scalar_blocks/-/blob/master/Install.md)

 Return to your home directory `$HOME` and follow the instructions in Install.md. 

 ### blocks_3d
  The package blocks-3d 
 [https://gitlab.com/bootstrapcollaboration/blocks_3d](https://gitlab.com/bootstrapcollaboration/blocks_3d) is a C++ library computing general conformal blocks of spinning operators in d=3. You can find installation instuctions following this link [blocks_3d/Install.md](https://gitlab.com/bootstrapcollaboration/blocks_3d/-/blob/master/Install.md)

 
 Return to your home directory `$HOME` and  follow the instructions in Install.md. 

## Hyperion Installation 
The following packages  contain haskell programs for numerical conformal bootstrap computations. 
 - [ ] [https://gitlab.com/davidsd/scalars-3d](https://gitlab.com/davidsd/scalars-3d), 
 - [ ] [https://gitlab.com/davidsd/fermions-3d](https://gitlab.com/davidsd/fermions-3d) 
 - [ ] [https://gitlab.com/davidsd/dynamical-sdp](https://gitlab.com/davidsd/dynamical-sdp) 
In pariticular, `scalars-3d` contains examples of setting up computations of Bootstrap Mixed Correlators of Ising Model and O(N) model. Similarly, `fermions-3d` contains examples of setting up fermionic bootstrap and GNY model. Finally, `dynamical-sdp` is based on the former two packges and contains examples of using skydiving algorithm. 

Go to the directory `$HOME/hyperion-projects` 
```
git clone https://gitlab.com/davidsd/scalars-3d.git
git clone https://gitlab.com/davidsd/fermions-3d.git
git clone https://gitlab.com/davidsd/dynamical-sdp.git

``` 
then `cd` into `scalars-3d`, `fermions-3d` and `dynamical-sdp`. The steps are almost the same. So please follow the lines below for all three packges. 

First of all, make your own configuration scripts by either copying from the existing ones. 

```
cp -r hyperion-config/caltech-hpc-dsd hyperion-config/your-cluster-your-name
```
Edit 'hyperion-config/your-cluster-your-name/Config.hs' with the path to your scripts
```
scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "your-cluster-your-name" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "your work directory to store the output")
  { emailAddr = Just "your email adress if you want to receive notifications" }
```

Edit the scripts in 'hyperion-config/your-cluster-your-name' to include the paths to the executables available on your cluster. (The correct verion of the skydiving executable is called "dynamical_sdp_RV1B". Path to this executable should be included in your srun_dynamical_sdp.sh)
```
mkdir hyperion-config/src
cd hyperion-config/src
ln -s ../your-cluster-your-name/Config.hs Config.hs

```
After finishing steps above, try to compile the code using `stack` commands 
```
# For those with a freshly set up PI-Symmetry account, check your stack version first. If it is too old, do stack --upgrade 
stack build 
stack install 
```
Finally, in `scalars-3d`  try calling 
```
./hyp IsingSigEpsAllowed_test_nmax6
```
and in `fermions-3d` 
```
./hyp FourFermions3dAllowed_test_nmax6
```
and in `dynamical-sdp`
```
./hyp testDynamicalSdpIsingTheta
``` 
You will find your job information in the file "nohup.out", which include paths to the database and master log file of the program. The master log takes the following format
```
 /gpfs/aliu/hyperion-test/logs/2023-04/jPERH.log
```
You can then find the work logs with solver details,
```
 /gpfs/aliu/hyperion-test/logs/2023-04/jPERH/0.log
```

### Possible errors.
On Caltech HPC, during compilation we have seen the following errors occuring 
```
error: undefined reference to ‘mpfr_gamma_inc’
error: undefined reference to ‘mpfr_beta’
```
The solution is to delete the line `- /lib64` in the file, stack.yaml. 

Also on Caltech cluster, we have encountered runtime error 
```
error while loading shared libraries: libmpfr.so.6: cannot open shared object file:
```
You can try append this path `/home/aliu7/lib` to your `LD_LIBRARY_PATH`.
## Simpleboot Installation 

Simpleboot is a Mathematica framework for bootstrap computations. Simpleboot uses several C++ packages, which need to be installed as well. In the following sections, we will first install all the C++ packages, then configure simpleboot to use those packages. 

### Install SDPB and related packages

Simpleboot uses modified versions of SDPB, scalar_blocks, sdp2input. First make sure the installation in section “SDPB, Scalar-blocks, Blocks-3d Installation” is done properly. Then the installation of the rest packages are quite similar. 

1, Install https://github.com/suning-git/sdpb/tree/sdpb2.4.0_midck_stallingrecover. The installation is the same as SDPB 2.5.0, except it does not use libarchive packages.

Example :
```
cd $HOME/packages
mkdir -p sdpb2.4.0_midck
cd sdpb2.4.0_midck
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdpb2.4.0_midck_stallingrecover
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdpb2.4.0_midck/install
% will be installed to $HOME/packages/sdpb2.4.0_midck/install/bin/sdpb2.4.0_midck
./waf install
```

2, Install https://github.com/suning-git/sdpb/tree/sdpdd_bBcA_hessian. The installation is the same as SDPB 2.5.0, except it does not use libarchive packages.

Example:
```
cd $HOME/packages
mkdir -p sdpdd_bBcA_hessian
cd sdpdd_bBcA_hessian
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdpdd_bBcA_hessian
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdpdd_bBcA_hessian/install
% will be installed to $HOME/packages/sdpdd_bBcA_hessian/install/bin/sdpdd_hessian_ij
./waf install
```

3, Install https://github.com/suning-git/sdpb/tree/sdp2input_mod. The installation is the same as SDPB 2.5.0, except it does not use libarchive packages.

Example:
```
cd $HOME/packages
mkdir -p sdp2input_mod_2.4.0
cd sdp2input_mod_2.4.0
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdp2input_mod
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdp2input_mod_2.4.0/install
% will be installed to $HOME/packages/sdp2input_mod_2.4.0/install/bin/sdp2input_mod_2.4.0
./waf install
```

4, Install https://github.com/suning-git/sdpb/tree/sdp2input_mod_2.5.0. The installation is the same as SDPB 2.5.0.

Example:
```
cd $HOME/packages
mkdir -p sdp2input_mod_2.5.0
cd sdp2input_mod_2.5.0
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdp2input_mod_2.5.0
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdp2input_mod_2.5.0/install
% will be installed to $HOME/packages/sdp2input_mod_2.5.0/install/bin/sdp2input_mod_2.5.0
./waf install
```

5, Install https://gitlab.com/suning-git/scalar-blocks-mod. The installation is similar to scalarblocks. The difference is that this code also depends on elemental.

Example:
```
cd $HOME/packages
mkdir -p scalarblocks_mod
cd scalarblocks_mod
git clone https://gitlab.com/suning-git/scalar-blocks-mod.git
cd scalar-blocks-mod

./waf configure --prefix=$HOME/packages/scalarblocks_mod/install --trilinos-dir=$HOME/packages/install --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install
./waf install
% will be installed to $HOME/packages/scalarblocks_mod/install/bin/scalar_blocks_mod
```


### Troubleshooting

The elemental-log package requires mpfr while the original elemental package in Walter’s installation code doesn’t require mpfr. On some clusters, you might have to modify Walter’s script by explicitly specifying the mpfr library and includes when using cmake to compile the file.

On some clusters, the gcc compiler might run into errors in threading mode. The multi-threading compilation might be specified as “./b2 --prefix=$HOME/packages/install -j 32” or “./waf -j 32”, where “-j 32” tells the compiler to use 32 CPU cores. To solve it, you may run the command with “-j 1” which turns off the multi-threading.

On some clusters, if compiled with boost 1.7.x, SDPB and related packages (skydiving, modified SDPB) have a bug that loading binary checkpoint might get stuck. If this happens, please recompile the packages with boost 1.6.7.

### Automatic SSH

Simpleboot can be used to control remote clusters. Simpleboot communicates with clusters through SSH and the users can directly submit jobs and download results within simpleboot without explicitly commanding in SSH. To use simpleboot in this way, the SSH login to the cluster has to be automatized (i.e. without manually input password every time). There are two ways to do that:

(1), If the cluster doesn’t require two-factor authentication (usually requires you to manually enter a code that is generated on your mobile phone), you can following the instructions here http://www.linuxproblem.org/art_9.html to configure the SSH client.

(2), If the cluster requires two-factor authentication, the only way to avoid entering code every login is through the ControlMaster feature in SSH, which requires you to enter the code once then reuse the connection for all other SSH sessions. Please follow the instructions here https://docs.rc.fas.harvard.edu/kb/using-ssh-controlmaster-for-single-sign-on/ to configure the SSH client. 
Unfortunately the ControlMaster feature is not available on Windows systems. In this case you will not be able to use simpleboot to control remote clusters. 

Note: Perimeter/EPFL clusters are in case (1). Caltech/Harvard clusters are in case (2).

### Configure simpleboot on a local computer/laptop

Download the package https://gitlab.com/bootstrapcollaboration/simpleboot to local laptop/desktop.

The folder Workspace_example is an example of simpleboot workspace. The workspace is the directory that hosts your specific bootstrap projects (you can have multiple workspace directories), while the simpleboot package code is in a separate folder. First we need to configure the workspace to work properly.

Edit Workspace_example\Scripts\config.m to specify the cluster information, location of the various packages.

```
    "[Cluster.LoginServer]" -> "10.10.21.98",   (* The address of your cluster *)
    "[Cluster.Account]" -> "nsu2",  (* Your account. Simpleboot will use "ssh [Cluster.Account]@[Cluster.LoginServer]" to connect to the cluster  *)
    "[Cluster.WorkspaceDirectory]" -> "/gpfs/nsu2/simpleboot3.1_workspace",     (* Workspace directory on the cluster *)
    "[Cluster.PackageDirectory]" -> "/gpfs/nsu2/simpleboot3.1_package/Packages",     (* Packages directory on the cluster *)
    "[Local.PackageDirectory]" -> Environment["SB3PACKAGEDIR"], (* Packages directory on the your laptop. If you prefer to use dropbox/OneDrive to host the file and use multiple computers to work on the project, you may set an environment variable for it. *)
    
    "[AutoCB3.scalar_blocks_mod.script]"->
    "mpirun --bind-to none -n 1 /gpfs/nsu2/bootstrap_bin/scalar_blocks_mod --num-threads $phys_cores_per_node", (* The script to call scalar_blocks_mod *)
    "[AutoCB3.sdp2input_mod.script]"->
    "mpirun -n $phys_cores_per_node --bind-to none /gpfs/nsu2/bootstrap_bin/sdp2input_mod_2.5.0 ", (* The script to call sdp2input_mod *)
    "[AutoCB3.sdpdd]"->
    "mpirun -n $phys_cores_per_node /gpfs/nsu2/bootstrap_bin/sdpdd --procsPerNode=$phys_cores_per_node ", (* The script to call sdpdd *)
    "[sdpb.script]"->
    "mpirun -n $phys_cores_per_node /gpfs/nsu2/bootstrap_bin/sdpb2.5.1 --procsPerNode $phys_cores_per_node", (* The script to call SDPB *)
    "[DynamicSDPB.script]"->
    "mpirun -n $phys_cores_per_node /gpfs/nsu2/bootstrap_bin/dynamical_sdp_RV1B", (* The script to call skydiving *)
```
Edit config.sh . This file should load modules and set necessary environment variables for SDPB. This file will be executed at the beginning of every simpleboot job. Alternative, you can put those scripts to $HOME/.bashrc2, which will be executed whenever one login to the cluster.

Edit RunMMA_job.sh first 4 lines. Those are the SLURM cluster job configuration script. You might want to change the number of nodes, specify the partition and the time.

### Install simpleboot on a remote cluster

If the simpleboot is configured to communicate with the cluster in automatic mode (i.e. through automatic SSH connection), the installation of the simpleboot package on the cluster is easy.
Open Example_controlCluster.nb and execute
```
Simpleboot$Installer$Packages[];
Simpleboot$Installer$Workspace[];
```
The simpleboot packages will be upload to "[Cluster.PackageDirectory]" and a workspace folder will be set up at "[Cluster.WorkspaceDirectory]", where the directories are specified in Scripts/config.m

If automatic SSH connection doesn't work for you, you can still manually operate simpleboot on the cluster through SSH. We will show you how to do that during the mini-course.

## For Perimeter users

Simpleboot and all dependent packages are already installed. To use simpleboot:

1, Following "Automatic SSH" section to configure SSH. 

2, Download the package https://gitlab.com/bootstrapcollaboration/simpleboot to local laptop/desktop. Copy the Scripts folder from Scripts_Examples\Perimeter to the Workspace_example folder.

3, Edit Workspace_example/Scripts/config.m . Change only the following items:


```
    "[Cluster.Account]" -> "nsu2",  (* Your account ID *)
    "[Cluster.WorkspaceDirectory]" -> "/gpfs/nsu2/simpleboot3.1_workspace",     (* choose an empty folder on the cluster *)
    "[Local.PackageDirectory]" -> Environment["SB3PACKAGEDIR"], (* Packages directory on the your laptop. If you prefer to use dropbox/OneDrive to host the file and use multiple computers to work on the project, you may set an environment variable for it. *)
```
Now you are ready to use simpleboot.

## For Caltech users
Simpleboot and all dependent packages are already installed. We will provide Scripts later.

## An example of a full installation of all C++ packages

```
# load modules
module load gcc/9.2.0-fasrc01 cmake/3.16.1-fasrc01 OpenBLAS/0.3.7-fasrc02 openmpi/4.0.2-fasrc01 eigen/3.3.7-fasrc02 libxml2/2.7.8-fasrc02 metis/5.1.0-fasrc01
module unload boost
module load mpfr/4.1.0-fasrc01

# install elemental-log
cd $HOME/packages
git clone https://gitlab.com/AikeLiu/elemental.git
cd elemental
git checkout MPFR_LogExp
mkdir -p build
cd build
export CXX=mpicxx
export CC=mpicc
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/packages/install -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc -DMETIS_INCLUDE_DIR=/n/helmod/apps/centos7/Core/metis/5.1.0-fasrc01/include -DMETIS_LIBRARY=/n/helmod/apps/centos7/Core/metis/5.1.0-fasrc01/lib/libmetis.a -DMPFR_INCLUDES=/n/helmod/apps/centos7/Core/mpfr/4.1.0-fasrc01/include -DMPFR_LIBRARIES=/n/helmod/apps/centos7/Core/mpfr/4.1.0-fasrc01/lib64/libmpfr.so
make && make install

# install boost
cd $HOME/packages
mkdir -p boost1.68
cd boost1.68
wget https://boostorg.jfrog.io/artifactory/main/release/1.68.0/source/boost_1_68_0.tar.bz2
tar --bzip2 -xf boost_1_68_0.tar.bz2
cd boost_1_68_0
./bootstrap.sh --prefix=$HOME/packages/install --without-libraries=python
./b2 --prefix=$HOME/packages/install -j 1
./b2 --prefix=$HOME/packages/install install

# install rapidjson
cd $HOME/packages
git clone https://github.com/Tencent/rapidjson.git
cd rapidjson
mkdir -p build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/packages/install
make && make install

# install libarchive
cd $HOME/packages
wget http://www.libarchive.org/downloads/libarchive-3.5.1.tar.xz
tar -xf libarchive-3.5.1.tar.xz
cd libarchive-3.5.1
./configure --prefix=$HOME/packages/install
make -j 16
make install

# install Trilinos
cd $HOME/packages
git clone --branch trilinos-release-12-12-branch https://github.com/trilinos/Trilinos.git
mkdir -p Trilinos/build
cd Trilinos/build
cmake -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DTrilinos_ENABLE_Sacado=ON -DTrilinos_ENABLE_Kokkos=OFF -DTrilinos_ENABLE_Teuchos=OFF -DCMAKE_INSTALL_PREFIX=$HOME/packages/install ..

# install scalarblocks
cd $HOME/packages
git clone https://gitlab.com/bootstrapcollaboration/scalar_blocks.git
cd scalar_blocks
./waf configure --prefix=$HOME/packages/install --trilinos-dir=$HOME/packages/install --boost-dir=$HOME/packages/install
./waf install

# compile SDPB2.5.1
cd $HOME/packages
mkdir -p sdpb2.5.1
cd sdpb2.5.1
git clone https://github.com/davidsd/sdpb.git
cd sdpb
git checkout 2.5.1
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --libarchive-dir=$HOME/packages/install --prefix=$HOME/packages/install
# sdpb will be installed to $HOME/packages/install/bin/sdpb
./waf install
# use "$HOME/packages/install/bin/sdpb --version" to check if it compiled correctly

# compile skydiving
cd $HOME/packages
mkdir -p skydiving1.0
cd  skydiving1.0
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout skydiving_release
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --libarchive-dir=$HOME/packages/install --prefix=$HOME/packages/install
# sdpb will be installed to $HOME/packages/install/bin/dynamical_sdp_RV1B
./waf install

# install modified SDPB packages. Those are not official package. So I will set the install packages to customized location, in order to not mix with the official version

# compile sdpb2.4.0_midck 
cd $HOME/packages
mkdir -p sdpb2.4.0_midck
cd sdpb2.4.0_midck
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdpb2.4.0_midck_stallingrecover
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdpb2.4.0_midck/install
# will be installed to $HOME/packages/sdpb2.4.0_midck/install/bin/sdpb2.4.0_midck
./waf install

# compile sdpdd
cd $HOME/packages
mkdir -p sdpdd_bBcA_hessian
cd sdpdd_bBcA_hessian
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdpdd_bBcA_hessian
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdpdd_bBcA_hessian/install
# will be installed to $HOME/packages/sdpdd_bBcA_hessian/install/bin/sdpdd_hessian_ij
./waf install

# compile sdp2input_mod_2.4.0
cd $HOME/packages
mkdir -p sdp2input_mod_2.4.0
cd sdp2input_mod_2.4.0
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdp2input_mod
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdp2input_mod_2.4.0/install
# will be installed to $HOME/packages/sdp2input_mod_2.4.0/install/bin/sdp2input_mod_2.4.0
./waf install

# compile sdp2input_mod_2.5.0
cd $HOME/packages
mkdir -p sdp2input_mod_2.5.0
cd sdp2input_mod_2.5.0
git clone https://github.com/suning-git/sdpb.git
cd sdpb
git checkout sdp2input_mod_2.5.0
./waf configure --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install --rapidjson-dir=$HOME/packages/install --prefix=$HOME/packages/sdp2input_mod_2.5.0/install
# will be installed to $HOME/packages/sdp2input_mod_2.5.0/install/bin/sdp2input_mod_2.5.0
./waf install

# compile scalarblocks_mod
cd $HOME/packages
mkdir -p scalarblocks_mod
cd scalarblocks_mod
git clone https://gitlab.com/suning-git/scalar-blocks-mod.git
cd scalar-blocks-mod

./waf configure --prefix=$HOME/packages/scalarblocks_mod/install --trilinos-dir=$HOME/packages/install --boost-dir=$HOME/packages/install --elemental-dir=$HOME/packages/install
./waf install
# will be installed to $HOME/packages/scalarblocks_mod/install/bin/scalar_blocks_mod
```

- [ ] [Set up project integrations](https://gitlab.com/AikeLiu/haskell-crash-course/-/settings/integrations)
