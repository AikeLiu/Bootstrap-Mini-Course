module Exercise  where 

doYouEnjoyHaskell :: IO ()
doYouEnjoyHaskell = do
  putStrLn "Are you enjoying Haskell? (yes/no)"
  answer <- getLine
  case answer of
    "yes" -> putStrLn "Fantastic! I promise you'll enjoy it more after the exercises :)"
    _ -> do
      putStrLn "Invalid input. Let's try again..."
      doYouEnjoyHaskell


-- The following exercise are taken from <Yet Another Haskell Tutorial>. The pdf can be found online


-- Ex 3.6 (function composition)
-- Write a function that takes a list of pairs of length at least 2 and returns the first component of the second element in the list. 
-- So, when provided with [(5,’b’),(1,’c’),(6,’a’)], it will return 1. 


-- Ex 3.7 (Recursion)
-- Write a recursive function fib that takes a positive integer n as a parameter and calculates Fn


-- Ex 4.3 (Types)


-- Ex 4.6 and 4.7 (Self defined types) 


-- Ex: Declare your Tuple type from last exercise an instance of Eq Class  


-- Ex 7.2 
-- The function and takes a list of booleans and returns True if and only if all of them are True. It also returns True on the empty list. 
-- We did not cover foldr. You can read about it and then Write this function in terms of foldr. Or you can write it from scratch


-- Ex 9.2 Monad 
-- Monad is not an intuitive concept at first. I recommend you read through the Chapter first and then try Exercise 9.2

 





