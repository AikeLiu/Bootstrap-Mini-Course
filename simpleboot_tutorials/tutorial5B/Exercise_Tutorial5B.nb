(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16240,        358]
NotebookOptionsPosition[     15535,        333]
NotebookOutlinePosition[     15907,        349]
CellTagsIndexPosition[     15864,        346]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Exercise 1", "Subsection",
 CellChangeTimes->{{3.8915895536169076`*^9, 
  3.891589558592167*^9}},ExpressionUUID->"962fa784-6357-4e69-a4ab-\
0c3da4abe07b"],

Cell[TextData[{
 "Use testrun_skydiving_actual.nb to run Ising ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Sigma]", ",", "\[Epsilon]"}], "}"}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "ccf0f913-397f-4de2-8a93-b21c696821a1"],
 " using skydiving algorithm"
}], "Text",
 CellChangeTimes->{{3.890943094624915*^9, 3.890943153143484*^9}, {
  3.890943526869833*^9, 3.890943716463705*^9}, {3.8909437508438396`*^9, 
  3.8909439229734545`*^9}, {3.8913023133891716`*^9, 3.891302349047311*^9}, {
  3.8915895228666496`*^9, 3.8915896014764147`*^9}, {3.891729767593129*^9, 
  3.8917298207424746`*^9}},ExpressionUUID->"e1f3d74c-283f-4993-8615-\
839f85404401"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Exercise 2 (final exercise)", "Subsection",
 CellChangeTimes->{{3.8915895536169076`*^9, 3.891589558592167*^9}, 
   3.8917298262585793`*^9, {3.891730454753294*^9, 
   3.8917304649291477`*^9}},ExpressionUUID->"b68daefc-f489-4cda-9395-\
f61d68a670d4"],

Cell[TextData[{
 "Follow the Wednesday and Friday simpleboot tutorials to implement ",
 Cell[BoxData[
  FormBox[
   RowBox[{"O", "(", "3", ")"}], TraditionalForm]],ExpressionUUID->
  "3ffddf25-08c4-4694-8e84-148e1efc40ba"],
 " ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "s", ",", "t"}], "}"}], TraditionalForm]],
  ExpressionUUID->"2fb751ca-2062-4523-9380-8359164a7211"],
 " system from scratch (you can copy/paste some command from other tutorial \
examples). Use autoboot to generate the bootstrap equation. Scan the ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "s", ",", "t", ",", 
     RowBox[{
      RowBox[{"all", " ", "OPEs"}], "..."}]}], "}"}], TraditionalForm]],
  ExpressionUUID->"3b3ad6e7-a307-4948-95be-ed7a948b5d50"],
 " using skydiving algorithm. Maximize ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["\[CapitalDelta]", "4"], TraditionalForm]],ExpressionUUID->
  "b1b7d138-5f93-4fa1-a47d-acdd8a2ddc5c"],
 " at ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalLambda]", "=", "19"}], TraditionalForm]],ExpressionUUID->
  "652ed247-ade0-4c45-ba32-65226061cb52"],
 ".\n\nStep 1: Implement ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "s"}], "}"}], TraditionalForm]],ExpressionUUID->
  "c23a53e9-aed9-449d-95a4-132857e51790"],
 " without OPE scan as feasibility test, i.e. leave ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["V", "\[Theta]"], TraditionalForm]],ExpressionUUID->
  "89c3cd76-8975-4548-8a09-5fa0a64c4de3"],
 " term as a 2\[Times]2 matrix and the theory space is ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "s"}], "}"}], TraditionalForm]],ExpressionUUID->
  "06bf0944-b35d-4749-9ed4-78117c60b324"],
 ". Check a few points to make sure you see an O(5) island. Note: the O(3) \
island doesn\[CloseCurlyQuote]t exist at ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalLambda]", "=", "11"}], TraditionalForm]],ExpressionUUID->
  "c00ab565-8bb8-4d0e-b7d8-494729cb69c0"],
 ". You will need ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalLambda]", "=", "19"}], TraditionalForm]],ExpressionUUID->
  "25e95c75-3971-4129-8c63-426fdce2d224"],
 ". You should use autoboot to generate the bootstrap equation. The command \
for that is in autoboot_demo.nb. \nNote : the autoboot rep name is\nv[0, 1] : \
singlet\nv[1,-1] : vector\nv[2, 1] : traceless symmetric tensor\nv[4, 1] : \
rank 4 tensor\n\nStep 2: Modify your setup in Step 1 to include OPE scan, \
i.e. the theory space is ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "s", ",", 
     RowBox[{
      SubscriptBox["\[Lambda]", "\[Phi]\[Phi]s"], "/", 
      SubscriptBox["\[Lambda]", "sss"]}]}], "}"}], TraditionalForm]],
  ExpressionUUID->"796ad780-34cb-4113-9267-ded5954412f1"],
 ". Use navigator run to minimize ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["\[CapitalDelta]", "\[Phi]"], TraditionalForm]],
  ExpressionUUID->"13b05d25-8eb0-491a-a052-5335b05dbd01"],
 ".\nStep 3: Modify your file in Step 2 and use skydiving run to maxmize ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["\[CapitalDelta]", "\[Phi]"], TraditionalForm]],
  ExpressionUUID->"0ca39a17-6f48-4c22-8729-8a0b05f49de8"],
 ". For the skydiving parameters, you can use the following :\n\n",
 Cell[BoxData[
  FormBox[
   RowBox[{"\"\<DynamicSDPB.DualityGapUpperLimit\>\"", "\[Rule]", 
    RowBox[{"Automatic", "[", 
     SuperscriptBox["10", 
      RowBox[{"-", "4"}]], "]"}]}], TraditionalForm]],
  FormatType->"TraditionalForm",ExpressionUUID->
  "062eccc6-0114-4a06-bffc-2ee1390249b2"],
 " \n\nand for the executable parameters:\n\n",
 Cell[BoxData[
  RowBox[{
  "\"\<--procsPerNode $phys_cores_per_node --centeringRThreshold=1.0e-10 \
--precision 768 --checkpointInterval 3600 --maxRuntime 72000 \
--dualityGapThreshold 1.0e-30 --primalErrorThreshold 1.0e-15 \
--dualErrorThreshold 1.0e-15 --initialMatrixScalePrimal 1.0e20 \
--initialMatrixScaleDual 1.0e20 --feasibleCenteringParameter 0.3 \
--infeasibleCenteringParameter 0.3 --stepLengthReduction 0.7 \
--maxComplementarity 1.0e100 --maxIterations 1000 --verbosity 1 \
--procGranularity 1 --findBoundaryObjThreshold 1.0e-20 \>\"", "<>", 
   "\[IndentingNewLine]", 
   "\"\<--maxClimbingSteps 1 --betaScanMin=0.3 --betaScanMax=1.01 \
--betaScanStep=0.1 \>\"", "<>", "\[IndentingNewLine]", 
   "\"\<--stepMinThreshold=0.1 --stepMaxThreshold=0.6 \
--primalDualObjWeight=0.2 --navigatorWithLogDetX=False \
--gradientWithLogDetX=True --betaClimbing 1.5 \>\""}]], "Input",
  CellChangeTimes->{{3.855600557574055*^9, 3.855600570110359*^9}, {
    3.855606854096895*^9, 3.8556068549170923`*^9}, {3.855609103400367*^9, 
    3.855609105077815*^9}, 3.855671043244536*^9, {3.855680978686578*^9, 
    3.8556810141094494`*^9}, {3.855774166685508*^9, 3.8557741809682274`*^9}, 
    3.8557752476570816`*^9, {3.855776280940817*^9, 3.85577628236882*^9}, 
    3.8557770704634037`*^9, {3.85885756741504*^9, 3.8588575970726643`*^9}, {
    3.858857651269051*^9, 3.858857651671356*^9}, {3.8588639940623503`*^9, 
    3.858863994190567*^9}, 3.858877276323751*^9, 3.8588949001755533`*^9, 
    3.859749892256708*^9, {3.859749939702633*^9, 3.859749943169473*^9}, {
    3.860734401515653*^9, 3.8607344019458942`*^9}, {3.8607344699111176`*^9, 
    3.860734474484912*^9}, 3.8607345093029346`*^9, {3.8607345601831055`*^9, 
    3.860734561437029*^9}, 3.860779192484133*^9, {3.862156459649935*^9, 
    3.8621564636318426`*^9}, {3.862327615442866*^9, 3.8623276178458157`*^9}, 
    3.8624764984021635`*^9, 3.862477274308045*^9, 3.862537853336192*^9, {
    3.864298795211482*^9, 3.8642988282426414`*^9}, {3.864721845772973*^9, 
    3.8647218522037306`*^9}, {3.8647227091290784`*^9, 
    3.8647227098479753`*^9}, {3.8647329920612783`*^9, 3.864733011501667*^9}, {
    3.8647443067640443`*^9, 3.864744306996767*^9}, {3.8648192767890615`*^9, 
    3.864819285522457*^9}, {3.8657503169993944`*^9, 3.865750325214103*^9}, 
    3.8657510847837534`*^9, {3.8657511274392886`*^9, 3.865751128636529*^9}, {
    3.8658546317125053`*^9, 3.865854632299506*^9}, {3.86604451444571*^9, 
    3.866044529027804*^9}, {3.8660448126912036`*^9, 3.8660448158415704`*^9}, 
    3.8684441846981645`*^9, {3.8684442259115515`*^9, 
    3.8684442702415266`*^9}, {3.868444898093603*^9, 3.8684449298596244`*^9}, {
    3.868469022590705*^9, 3.868469023500244*^9}, {3.8685346171132665`*^9, 
    3.868534645568758*^9}, {3.869592120152299*^9, 3.8695921276590004`*^9}, {
    3.869592208918048*^9, 3.869592214200101*^9}, {3.8696663523437304`*^9, 
    3.869666355515128*^9}, {3.8707852269275064`*^9, 3.8707852288026705`*^9}, {
    3.8707876957376328`*^9, 3.870787696152686*^9}, {3.870787729948489*^9, 
    3.870787749544691*^9}, {3.8708793259417915`*^9, 3.870879327899617*^9}, {
    3.8708916396849403`*^9, 3.8708916520924997`*^9}, {3.8708917162728286`*^9, 
    3.870891744471009*^9}, 3.8709238879961815`*^9, 3.8709240620344877`*^9, {
    3.8710204372182307`*^9, 3.8710204392140446`*^9}, 3.8710237462270107`*^9, {
    3.8710418159757423`*^9, 3.8710418182500534`*^9}, {3.8711804412970753`*^9, 
    3.871180453528518*^9}, 3.87118087608891*^9, {3.8711812629661245`*^9, 
    3.8711812635352187`*^9}, 3.8712262134798822`*^9, {3.8712263672765265`*^9, 
    3.8712263675599174`*^9}, 3.8712301093564677`*^9, 3.871303921217354*^9, {
    3.871313034011527*^9, 3.8713130349893436`*^9}, 3.8713575854092937`*^9, 
    3.871358940085492*^9, {3.8713654830041847`*^9, 3.8713654846807575`*^9}, 
    3.8716329176010184`*^9, {3.8716402438546743`*^9, 
    3.8716402476891413`*^9}, {3.8718053389291553`*^9, 3.8718053401949744`*^9},
     3.8721772845185905`*^9, 3.872250118615404*^9, {3.872311626644059*^9, 
    3.872311627627912*^9}, {3.8723116620083933`*^9, 3.872311667057004*^9}, {
    3.872746212922189*^9, 3.8727462154646034`*^9}, {3.8727462914208064`*^9, 
    3.87274629527133*^9}, 3.8728230525512857`*^9, {3.8732263877474527`*^9, 
    3.8732264105466375`*^9}, 3.8732265497573304`*^9, 3.873470577509472*^9, {
    3.8734707039796853`*^9, 3.8734707098019304`*^9}, {3.873730683131017*^9, 
    3.8737306853770103`*^9}, 3.8743356391077976`*^9, {3.874337222002968*^9, 
    3.874337235995011*^9}, {3.875548753889363*^9, 3.8755487540794144`*^9}, {
    3.8755919283777447`*^9, 3.8755919286716547`*^9}, {3.875990521297826*^9, 
    3.875990521552517*^9}, {3.87605176283739*^9, 3.876051762982218*^9}, {
    3.8784075971554613`*^9, 3.8784076344354973`*^9}, {3.878490042092338*^9, 
    3.878490043387271*^9}, {3.8786584652323093`*^9, 3.8786584717721105`*^9}, {
    3.878708588703287*^9, 3.878708591641036*^9}, {3.878708621757436*^9, 
    3.8787086238626127`*^9}, 3.8787470261774797`*^9, {3.878831448538689*^9, 
    3.878831449082591*^9}, 3.879232656815383*^9, {3.879232723915237*^9, 
    3.879232724867423*^9}, {3.8817508649970455`*^9, 3.8817509053313255`*^9}, 
    3.8818373578990326`*^9, {3.884164395402877*^9, 3.884164412943325*^9}, 
    3.8841955466056376`*^9, {3.8841955794486923`*^9, 3.884195580551096*^9}, 
    3.8841956561029396`*^9, 3.884195946867908*^9, {3.886310470689232*^9, 
    3.8863104958004494`*^9}, {3.886310560876875*^9, 3.8863105746618533`*^9}, {
    3.886518789714749*^9, 3.886518789865796*^9}, {3.88808903988354*^9, 
    3.888089065719873*^9}, {3.888134902306451*^9, 3.8881349030614147`*^9}, 
    3.8882998000655394`*^9, 3.8883384754546547`*^9, 3.888338581700514*^9, 
    3.888754259030782*^9, {3.8887564575972586`*^9, 3.8887564670275636`*^9}, {
    3.888865325199415*^9, 3.8888653310989847`*^9}, {3.890054640760725*^9, 
    3.8900546510516467`*^9}, {3.890255952930537*^9, 3.890255958509433*^9}, 
    3.8902561801636677`*^9, {3.8903809408253937`*^9, 3.890380941025857*^9}},
  ExpressionUUID->"f0b20a94-5786-4508-acda-ef51352fb98b"],
 "\n\nStep 4: Modify your file, switch to ",
 Cell[BoxData[
  FormBox[
   RowBox[{"O", "(", "3", ")"}], TraditionalForm]],ExpressionUUID->
  "2745d35b-2ea2-4081-a9da-eb1e7f3085f6"],
 " ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "s", ",", "t"}], "}"}], TraditionalForm]],
  ExpressionUUID->"4aa88688-3525-46da-830b-9eaf86d42948"],
 " system . Generate the bootstrap equation using autoboot. Includes all OPEs \
in the scan (OPE$vtv,OPE$tts,OPE$ttt,OPE$sss) but normalize ",
 Cell[BoxData[
  FormBox[
   RowBox[{"OPE$vvs", "=", "1"}], TraditionalForm]],ExpressionUUID->
  "17de4566-0ed6-4570-a5af-c34c7b42b372"],
 ". Also include a gap in rank 4 tensor. The total theory space is ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "s", ",", "t", ",", 
     SubscriptBox["t", "4"], ",", "OPE$vtv", ",", "OPE$tts", ",", "OPE$ttt", 
     ",", "OPE$sss"}], "}"}], TraditionalForm]],ExpressionUUID->
  "e7210468-6455-44ca-bb03-8197be779998"],
 ".\nStep 5: Run Step 4 setup in feasibility test to check a few points. At ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalLambda]", "=", "19"}], TraditionalForm]],ExpressionUUID->
  "d104ee10-0299-4d15-8add-21b59159ee17"],
 " you should find ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{
    "0.5191243700690649`", ",", "1.5971496603122013`", ",", 
     "1.2104877475884412`", ",", "2.999858", ",", "3.0476432557146427`", ",", 
     "2.4251753797599624`", ",", "3.9931805153258857`", ",", 
     "0.5609755350034351`"}], "}"}], TraditionalForm]],ExpressionUUID->
  "062732ba-f80f-4487-acd5-2cb23f41703d"],
 " is feasible, but ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{
    "0.5191243700690649`", ",", "1.5971496603122013`", ",", 
     "1.2104877475884412`", ",", "2.9998595", ",", "3.0476432557146427`", ",",
      "2.4251753797599624`", ",", "3.9931805153258857`", ",", 
     "0.5609755350034351`"}], "}"}], TraditionalForm]],ExpressionUUID->
  "d2239364-624b-4223-9378-20bfe4f2b4be"],
 " is infeasible\nStep 6: Modify the file as a navigator step. Use skydiving \
run to maximize ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["t", "4"], TraditionalForm]],ExpressionUUID->
  "94197dcb-749c-40b1-9644-63b615303c25"],
 " at ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalLambda]", "=", "11"}], TraditionalForm]],ExpressionUUID->
  "c5af8e63-90b9-4551-85e4-165ecfdb9f0e"],
 ". Take initial point to be ",
 Cell[BoxData[
  FormBox[
   RowBox[{"{", 
    RowBox[{
    "0.5189568486456733`", ",", "1.5953881140694308`", ",", 
     "1.2096966725922966`", ",", "2.977566987863731`", ",", 
     "3.046096409596078`", ",", "2.4232989444312243`", ",", 
     "3.989971985056043`", ",", "0.5574634725742268`"}], "}"}], 
   TraditionalForm]],ExpressionUUID->"f60c7730-9bd4-4bf0-9a5f-e165030bf7d6"],
 ". This run should take less than 1 day.\nStep 7: After step 6 is correctly \
finished. Use skydiving run to maximize ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["t", "4"], TraditionalForm]],ExpressionUUID->
  "e7bb929d-a0b4-41cb-980e-18a0dc1b3d22"],
 " at ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalLambda]", "=", "19"}], TraditionalForm]],ExpressionUUID->
  "7af88006-b65d-45c3-8ad1-555aa64016e3"],
 ". For the parameters, use the parameter in Step 3. This run will take about \
3 days. You should proof ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["t", "4"], "<", "3"}], TraditionalForm]],ExpressionUUID->
  "686bb10e-86ef-41dc-b48f-1847031982a2"],
 " !\n\nI give you an answer file in Answer_O3vst_max_t4. However try your \
best before look at the answer file."
}], "Text",
 CellChangeTimes->CompressedData["
1:eJxTTMoPSmViYGAQB2IQ7ff/cl/S3zeOOdNVpoHoNVdvHwHT1/rugOg9DLcf
gOgutfhvIFqiROer4L83jpOaF/4E0TN2RxvNA9Kn7PRtQPSrX5/WPALSLDI7
toHoY+vP7QLRt8p+7gfRMgEzzoFop6+vL4HoNe+2XAPRb7of/wPRk84tZH0M
pC+YM4qC6IyCdbYgepHTcScQ/UH7txuItlSuDwLRsSWHI0D0Nd0dWSB6yxGu
QhCt1hlVBaI/lV5oAtEpRudXguk169eD6AmPVm8F0XqPW/eAaA71/EMgOlrw
8SUQnaNvfQNEM0wo+wmid7px/QLRCVY//oPobavPiT0B0hY5Ohog2mbJKR0Q
bbJ6m/NTIL05yDgARHvtqgsH0Rrst2NBNABunOu8
  "],ExpressionUUID->"6c798c6c-a09a-4586-accf-bb7f0811c24b"]
}, Open  ]]
},
WindowSize->{1280, 555},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
Magnification:>1.2 Inherited,
FrontEndVersion->"11.1 for Microsoft Windows (64-bit) (May 16, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 160, 3, 58, "Subsection", "ExpressionUUID" -> \
"962fa784-6357-4e69-a4ab-0c3da4abe07b"],
Cell[743, 27, 690, 15, 35, "Text", "ExpressionUUID" -> \
"e1f3d74c-283f-4993-8615-839f85404401"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1470, 47, 254, 4, 58, "Subsection", "ExpressionUUID" -> \
"b68daefc-f489-4cda-9395-f61d68a670d4"],
Cell[1727, 53, 13792, 277, 941, "Text", "ExpressionUUID" -> \
"6c798c6c-a09a-4586-accf-bb7f0811c24b"]
}, Open  ]]
}
]
*)

