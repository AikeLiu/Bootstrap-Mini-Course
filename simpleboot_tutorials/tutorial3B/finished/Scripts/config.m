(* ::Package:: *)

 Cluster$Configuration = {
    "[Cluster.LoginServer]" -> "10.10.21.98",   (* The address of your cluster *)
    "[Cluster.Account]" -> "nsu2",  (* Your account. Simpleboot will use "ssh [Cluster.Account]@[Cluster.LoginServer]" to connect to the cluster  *)
    "[Cluster.WorkspaceDirectory]" -> "/gpfs/nsu2/PITutorial3B/workspace",     (* Workspace directory on the cluster *)
    "[Cluster.PackageDirectory]" -> "/gpfs/nsu2/PITutorial3B/simpleboot/Packages",     (* Packages directory on the cluster *)
    
    "[Local.PackageDirectory]" -> "C:\\Users\\shinn\\OneDrive\\talks\\Perimeter_minicourse\\slides\\Tutorial3B_fresh\\simpleboot\\Packages", 
    (* simpleboot package directory for the current workspace. If this is on your laptop, it should be the simpleboot package directory on your laptop.
    If you prefer to use dropbox/OneDrive to host the file and use multiple computers to work on the project, you may set an environment variable for it. *)
    
    "[AutoCB3.scalar_blocks_mod.script]"->
    "mpirun --bind-to none -n 1 /gpfs/nsu2/bootstrap_bin/scalar_blocks_mod --num-threads $phys_cores_per_node ", (* The script to call scalar_blocks_mod *)
    "[AutoCB3.sdp2input_mod.script]"->
    "mpirun -n $phys_cores_per_node --bind-to none /gpfs/nsu2/bootstrap_bin/sdp2input_mod_2.5.0 ", (* The script to call sdp2input_mod *)
    "[AutoCB3.sdpdd]"->
    False, (* The script to call sdpdd *)
    "[sdpb.script]"->
    "mpirun -n $phys_cores_per_node /gpfs/nsu2/bootstrap_bin/sdpb2.5.1 --procsPerNode $phys_cores_per_node", (* The script to call SDPB *)
    "[DynamicSDPB.script]"->
    False, (* The script to call skydiving *)
    
    
    (* no need to modify the items below *)
    "[ClusterModel]" -> "srun",
    "[SDPB.Version]"->"sdpb_El",
    "[QUADRATICNET.SCRIPT]"->"/home/nsu2/packages/stack/stack-2.5.1-linux-x86_64/stack exec -- /home/nsu2/packages/quadratic-net-old/quadratic-net-exe --pvm2sdpExecutable /home/nsu2/packages/quadratic-net-old/quadratic-net-master/scripts/pvm2sdp.sh --sdpbExecutable /home/nsu2/packages/quadratic-net-old/quadratic-net-master/scripts/sdpb.sh --workDir tmp",
    "[QUADRATICNET.SWITCH]"->False,
    
    "[UsePackage.scalar_block]"->False,
    "[UsePackage.sdp2input]"->False,
    "[Debug.TimingStatistics]"->True,
    
    (* AutoCB3 configuration *)
    "[UsePackage.AutoCB3]"->True,
    "[AutoCB3.no-fake-poles]"->True,
    "[AutoCB3.sdp2input_mod]"->True,
    "[AutoCB3.scalar_blocks_mod.cmdprec]"->120,
    "[AutoCB3.scalar_blocks_mod.precision]"->1024,
    "[AutoCB3.sdp2input_mod.precision]"->1024
}


ReconfigCmd[str_]:=FixedPoint[StringReplace[#,Cluster$Configuration]&,str];
SSH$ReconfigCmd=ReconfigCmd;
