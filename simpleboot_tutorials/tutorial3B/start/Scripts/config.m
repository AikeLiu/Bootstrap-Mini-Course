(* ::Package:: *)

 Cluster$Configuration = {
    "[Cluster.LoginServer]" -> "10.10.21.98",   (* The address of your cluster *)
    "[Cluster.Account]" -> False,  (* Your account. Simpleboot will use "ssh [Cluster.Account]@[Cluster.LoginServer]" to connect to the cluster  *)
    "[Cluster.WorkspaceDirectory]" -> False,     (* Workspace directory on the cluster *)
    "[Cluster.PackageDirectory]" -> False,     (* Packages directory on the cluster *)
    
    "[Local.PackageDirectory]" -> False, 
    (* simpleboot package directory for the current workspace. If this is on your laptop, it should be the simpleboot package directory on your laptop.
    If you prefer to use dropbox/OneDrive to host the file and use multiple computers to work on the project, you may set an environment variable for it. *)
    
    "[AutoCB3.scalar_blocks_mod.script]"->
    False, (* The script to call scalar_blocks_mod *)
    "[AutoCB3.sdp2input_mod.script]"->
    False, (* The script to call sdp2input_mod *)
    "[AutoCB3.sdpdd]"->
    False, (* The script to call sdpdd *)
    "[sdpb.script]"->
    False, (* The script to call SDPB *)
    "[DynamicSDPB.script]"->
    False, (* The script to call skydiving *)
    
    
    (* no need to modify the items below *)
    "[ClusterModel]" -> "srun",
    "[SDPB.Version]"->"sdpb_El",
    "[QUADRATICNET.SCRIPT]"->"/home/nsu2/packages/stack/stack-2.5.1-linux-x86_64/stack exec -- /home/nsu2/packages/quadratic-net-old/quadratic-net-exe --pvm2sdpExecutable /home/nsu2/packages/quadratic-net-old/quadratic-net-master/scripts/pvm2sdp.sh --sdpbExecutable /home/nsu2/packages/quadratic-net-old/quadratic-net-master/scripts/sdpb.sh --workDir tmp",
    "[QUADRATICNET.SWITCH]"->False,
    
    "[UsePackage.scalar_block]"->False,
    "[UsePackage.sdp2input]"->False,
    "[Debug.TimingStatistics]"->True,
    
    (* AutoCB3 configuration *)
    "[UsePackage.AutoCB3]"->True,
    "[AutoCB3.no-fake-poles]"->True,
    "[AutoCB3.sdp2input_mod]"->True,
    "[AutoCB3.scalar_blocks_mod.cmdprec]"->120,
    "[AutoCB3.scalar_blocks_mod.precision]"->1024,
    "[AutoCB3.sdp2input_mod.precision]"->1024
}


ReconfigCmd[str_]:=FixedPoint[StringReplace[#,Cluster$Configuration]&,str];
SSH$ReconfigCmd=ReconfigCmd;
