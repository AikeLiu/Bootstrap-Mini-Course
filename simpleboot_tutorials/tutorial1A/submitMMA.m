(* ::Package:: *)

GetFileDirectory[]:=If[$InputFileName==="",NotebookDirectory[],DirectoryName@$InputFileName];
$MainFileScriptQ=$InputFileName=!="";
AppendTo[$Path,GetFileDirectory[]];
SetDirectory@GetFileDirectory[];

Get["./Scripts/config.m"];
Get[ReconfigCmd@"[Local.PackageDirectory]/Bootstrapper.m"];





Get["IsingOE.m"];


AutoCB3$SaveSDPTemplate@SDPTemplate$IsingOE[];

initpts={{0.5181489,1.412625}}~Join~GeneratePointsInRectangular[{0.515,0.523},{1.38,1.45},3,3]//SetPrec;

SB$FeasibilityScanner[
SDP$IsingOE, (* function that generates a SDP *)
initpts, (* initial points to scan *)"--maxIterations=1000 --dualityGapThreshold=1e-25 --primalErrorThreshold=1e-15 --dualErrorThreshold=1e-15 --precision=765 --initialMatrixScalePrimal=1e+20 --initialMatrixScaleDual=1e+20 --maxComplementarity=1e+70 --detectPrimalFeasibleJump --detectDualFeasibleJump ", (* SDPB parameters *)

{"Delaunay"}, (* scan method *)
200, (* maximal points to scan *)
False (* initial checkpoint. False means no initial checkpoint. *)
]
