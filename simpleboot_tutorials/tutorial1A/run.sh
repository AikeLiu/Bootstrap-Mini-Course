#!/bin/bash
#SBATCH --partition=defq
#SBATCH --nodes=1
#SBATCH --time=0-01:00:00
#SBATCH --output ./btjob-%j.out


echo Number of tasks : $SLURM_NTASKS
echo Job-id : $SLURM_JOB_ID


source ./Scripts/config.sh

MathKernel -script $1

