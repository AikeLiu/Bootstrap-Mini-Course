#!/bin/bash

module load slurm
module load mathematica/12.1.1
#module load mathematica/11.0.1
#module load gcc/7.2.0
#module load gcc
module load openmpi
#module load mpich/ge/gcc/64/3.2rc2-174-cm8.1 
#module load intel/mpi
module load openblas/dynamic/0.2.20 
#module load openmpi/gcc-9/64/4.0.3
#module load sdpb/2.4.0
#module load openmpi/gcc/64/3.1.4
module load boost/1.67.0
#module load sdpb/2.1.2+22
#module load openmpi/gcc-9/64/4.0.3
module load openmpi/gcc-9/64/4.0.5
module load python/3.7
module load trilinos/13.0.0

export PATH=/gpfs/nsu2/packages/sdp2input_fix:$PATH:/home/nsu2/packages/install/MPSolve-2.2:/home/nsu2/packages/spectrum-extraction/spectrum-extraction-master:/home/nsu2/packages/install/MPSolve/usr/local/bin/

export LD_LIBRARY_PATH=/home/nsu2/packages/install/elemental_log/lib/:$LD_LIBRARY_PATH:/home/nsu2/packages/lapack/lib/:/home/nsu2/packages/install/MPSolve/usr/local/lib/

export phys_cores_per_node=$(lscpu | awk '/^Core.s. per socket:/ {cores=$NF} /^Socket.s.:/ {sockets=$NF} END {print cores * sockets}')


